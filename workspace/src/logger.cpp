#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<string>
#include<iostream>
#include"Puntuaciones.h"

int main(){
int fd;
Puntuaciones puntos;

	unlink("FIFO"); //Se añade un unlink al principio para solucionar error de que no deja abrirlo porque ya existe.
	if (mkfifo("FIFO", 0600)<0) {
		perror("No puede crearse el FIFO");
		return 1;
	}
	/* Abre el FIFO */
	if ((fd=open("FIFO", O_RDONLY))<0) {
		perror("No puede abrirse el FIFO");
		return 1;
	}
	
	while (read(fd, &puntos, sizeof(puntos))==sizeof(puntos))  {
		if(puntos.lastWinner==1){
			sprintf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador1);
		}
		else if(puntos.lastWinner==2){
			sprintf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n",puntos.jugador2);
		}
		
	}
	close(fd);
	unlink("FIFO");
	return(0);

}
